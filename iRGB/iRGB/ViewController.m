//
//  ViewController.m
//  iRGB
//
//  Created by Sveta on 3/20/18.
//  Copyright © 2018 Alena. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


- (IBAction)changeSlider:(UISlider *)sender {
  self.redLabel.text = [NSString stringWithFormat:@"%.2f", self.redS.value];
  self.greenLabel.text = [NSString stringWithFormat:@"%.2f", self.greenS.value];
  self.blueLabel.text = [NSString stringWithFormat:@"%.2f", self.blueS.value];
   self.view.backgroundColor=
  [UIColor colorWithRed:self.redS.value/255.0 green:self.greenS.value/255.0 blue:self.blueS.value/255.0 alpha:1.0];

}
@end
