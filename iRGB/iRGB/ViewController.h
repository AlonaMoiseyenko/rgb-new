//
//  ViewController.h
//  iRGB
//
//  Created by Sveta on 3/20/18.
//  Copyright © 2018 Alena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISlider *redS;
@property (weak, nonatomic) IBOutlet UISlider *greenS;
@property (weak, nonatomic) IBOutlet UISlider *blueS;
@property (weak, nonatomic) IBOutlet UILabel *redLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenLabel;
@property (weak, nonatomic) IBOutlet UILabel *blueLabel;
- (IBAction)changeSlider:(UISlider *)sender;


@end

